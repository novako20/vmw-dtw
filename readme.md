### Zarovnávání DNA sekvencí převedených na časové řady

DNA sekvenci lze převést na časovou řadu tak, že jednotlivým bázím A,C,G,T přiřadíme váhy (např. A:1, C:2, G:-2, T:-1) a při čtení sekvence upravujeme sumu hodnot podle právě čtené báze. Aplikace by měla umožňovat vizualizaci časových řad vygenerovaných z DNA sekvencí, váhy by měly být nastavitelné (např. ve webovém formuláři) a podobnost časových řad bude řešena pomocí DTW (dynamic time warping) distance.

[Dokumentace](docs.md)
