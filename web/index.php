<?php
// Výpočet max 10 minut
ini_set('max_execution_time', 600);

// Funkce na vytvareni grafu, vystup png graf
function Plotting_Data($data, $a, $t, $c, $g, $names)
{
    require_once("lib/pchart/class/pData.class.php");
    require_once("lib/pchart/class/pDraw.class.php");
    require_once("lib/pchart/class/pImage.class.php");

    $myData = new pData();
    $tics_max = 1;
    $counter = 0;
    foreach ($data as $single_data)
    {
        if (strlen($single_data) > $tics_max)
        {
            $tics_max = strlen($single_data);
        }
        $split_data = str_split($single_data);
        $values = array();
        $suma = 0;
        array_push($values, $suma);
        foreach ($split_data as $single_char)
        {
            switch ($single_char)
            {

                case 'a':
                    $suma += $a;
                    array_push($values, $suma);
                    break;
                case 'c':
                    $suma += $c;
                    array_push($values, $suma);
                    break;
                case 'g':
                    $suma += $g;
                    array_push($values, $suma);
                    break;
                case 't':
                    $suma += $t;
                    array_push($values, $suma);
                    break;

            }
        }
        $myData->addPoints($values, $names[$counter]);
        $counter++;
    }
    $myData->setAxisName(0, "Suma ACGT bází");
    $myData->setAbscissaName("Délka DNA sekvence");
    $myData->setPalette("VSTUP",array("R"=>0,"G"=>0,"B"=>0));
    $myData->setSerieWeight("VSTUP",2);

    $myPicture = new pImage(1800,600,$myData);
    $myPicture->setFontProperties(array("FontName"=>"lib/pchart/fonts/sourcecodepro.ttf","FontSize"=>20));
    $myPicture->setGraphArea(120,40,1660,540);
    if ($tics_max > 100) {$myPicture->drawScale(array("LabelSkip"=>99, "Factors"=>array(50)));}
    else if ($tics_max > 10) {$myPicture->drawScale(array("LabelSkip"=>9, "Factors"=>array(10)));}
    else {$myPicture->drawScale();}
    $myPicture->drawLineChart();
    $myPicture->setFontProperties(array("FontName"=>"lib/pchart/fonts/sourcecodepro.ttf","FontSize"=>18));
    $myPicture->drawLegend(1680,40,array("Style"=>LEGEND_ROUND,"Mode"=>LEGEND_VERTICAL));
    $path = "plot/plot_" . date("YmdHis") . ".png";
    $myPicture->render($path);
    return $path;
}

function ParseFullname($fullname)
{
    $name = preg_replace('/^(([A-Z]?[a-z]+_)+).*uid(\d+)\/.+$/', '$1:$3', $fullname);
    $name = str_replace('_',' ',$name);
    return $name;
}

// Smazani starych vytvorenych grafu ze slozky plot
$files = glob('plot/plot*');
foreach($files as $file){
  if(is_file($file))
    unlink($file);
}

// Počáteční hodnoty při prvním spuštění
$raw_string = "";
$baze_a = "0";
$baze_c = "0";
$baze_g = "0";
$baze_t = "0";
$speed = "25";
$pocet_vysl = "10";
$pocet_grafu = "3";

// Dosazení původních hodnot, aby nedocházelo k resetování formuláře
$plots = array();
$error = false;
$comments = '<font color="green">Aplikace doběhla v pořádku!</font>';
$result_picture = 'src="img/result_ok.png" name="mat" value="M" width="30" height="30"';

if (isset($_POST['StringInput']) && $_POST['StringInput'] != '')
{
    $raw_string = $_POST['StringInput'];
}
else if (isset($_FILES['fileToUpload']) && file_exists($_FILES['fileToUpload']['tmp_name']))
{
}
else
{
    $error = true;
    $comments = '<font color="red">Zadejte vstupní sekvenci! (přímý vstup / soubor)</font>';
}
if (isset($_POST['vysl']))
{
    $pocet_vysl = $_POST['vysl'];
    if (!ctype_digit($_POST['vysl']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Počet výsledků musí být celé číslo!</font>';
    }
}
if (isset($_POST['grafu']))
{
    $pocet_grafu = $_POST['grafu'];
    if (!ctype_digit($_POST['grafu']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Počet sekvencí v grafu musí být celé číslo!</font>';
    }
    else
    {
        // Maximálně 20 sekvencí se rozumně vejde do jednoho grafu
        if ($pocet_grafu > 20) {$pocet_grafu = 20;}
        if ($pocet_grafu > $pocet_vysl) {$pocet_grafu = $pocet_vysl;}
    }
}
if (isset($_POST['speed']))
{
    $speed = $_POST['speed'];
    if (!ctype_digit($_POST['speed']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Oblast výpočtu musí být celé číslo!</font>';
    }
}
if (isset($_POST['A-baze']))
{
    $baze_a = $_POST['A-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['A-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze A musí být celé číslo!</font>";
    }
}
if (isset($_POST['C-baze']))
{
    $baze_c = $_POST['C-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['C-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze C musí být celé číslo!</font>";
    }
}
if (isset($_POST['G-baze']))
{
    $baze_g = $_POST['G-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['G-baze']) && !$error)
    {
        $error = true;
        $comments = "<font color='red'>Váha báze G musí být celé číslo!</font>";
    }
}
if (isset($_POST['T-baze']))
{
    $baze_t = $_POST['T-baze'];
    if (!preg_match('/^-?\d{1,6}$/', $_POST['T-baze']) && !$error)
    {
        $error = true;
        $comments = '<font color="red">Váha báze T musí být celé číslo!</font>';
    }
}

if ($error)
{
    $result_picture = 'src="img/result_err.png" name="mat" value="M" width="30" height="30"';
}
?>

<html>
    <head>
        <style type="text/css">
        body {
            background: url("img/dna.png") fixed;
            background-position: top right;
            background-repeat: no-repeat;
            background-size: contain;
        }
        img, input, .mid {
            display: inline-block;
            vertical-align: middle;
        }
        table {border-collapse: collapse;}
        th, td {
            padding: 10px;
            border-bottom: 1px solid #ddd;
        }
        th {
            font-weight: bold;
            text-align:  left;
        }
        tr {text-align: right;}
        tr:nth-child(odd)  {background-color: #fff;}
        tr:nth-child(even) {background-color: #f2f2f2;}
        </style>

        <meta charset="UTF-8">
        <title>Podobnost DNA</title>

        <script>
            function showcase() {
                document.getElementById("StringInput").value = "ATGCCCTATACCTATGATTATCCGCGCCCTGGTCTTACCGTTGACTGCGTGGTGTTTGGCCTAGACGAACAGATCGATCTCAAAGTCCTACTGATTCAGCGCCAGATTCCCCCTTTCCAGCATCAGTGGGCATTACCCGGAGGCTTTGTGCAGATGGATGAATCTTTAGAAGACGCGGCTCGCCGGGAGCTGCGAGAAGAAACGGGGGTTCAGGGTATTTTCCTAGAGCAGCTCTATACCTTTGGCGATTTGGGTCGAGATCCGCGCGATCGCATCATCTCCGTTGCCTACTACGCTCTAATCAACCTCATCGAATATCCTTTACAAGCCTCTACTGATGCTGAAGACGCAGCCTGGTACTCCATTGAGAATTTGCCATCTCTAGCTTTTGATCATGCTCAAATCTTGAAACAGGCCATCCGGCGATTGCAGGGCAAAGTTCGCTACGAACCGATTGGCTTCGAACTACTGCCGCAAAAATTTACACTCACCCAAATTCAGCAGCTTTACGAAACAGTTCTTGGCCATCCTCTAGATAAACGGAACTTTCGTAAGAAATTGCTAAAAATGGATCTCTTAATTCCCCTTGATGAGCAACAAACTGGAGTTGCTCATCGTGCTGCCAGACTCTATCAGTTCGACCAAAGCAAATACGAGCTATTGAAACAACAGGGATTTAACTTCGAGGTTTGA";
                document.getElementById("A-baze").value = "1";
                document.getElementById("C-baze").value = "-2";
                document.getElementById("G-baze").value = "3";
                document.getElementById("T-baze").value = "-4";
                document.getElementById("vysl").value = "10";
                document.getElementById("grafu").value = "3";
                document.getElementById("speed").value = "10";
            }
        </script>
    </head>

    <body>
        <form action="" enctype="multipart/form-data" method='post'>
        <p><h1>Podobnost DNA sekvencí převedených na časové řady</h1></p>
        <?php
            if (isset($_POST['run']))
            {
                echo '<img ' . $result_picture . '>&nbsp;&nbsp;<span class="mid">' . $comments . '</span><br>';
            }
        ?>
        <p><h2>&#x2460; VSTUP</h2></p>
        <button type="button" onclick="showcase()">UKÁZKOVÝ VSTUP</button>
        <p><h3>Přímý vstup</h3></p>
        <textarea class="FormElement" name="StringInput" id="StringInput" style="width: 50%; height: 12em;" placeholder="ACGTACGTACGTACGT..." spellcheck="false"><?php echo $raw_string?></textarea>
        <br>
        <p><h3>Vstup ze souboru</h3></p>
        <input type="file" name="fileToUpload" value="">
        <br><br>

        <p><h2>&#x2461; VÁHY</h2></p>

        <p><h3>Váhy ACGT bází</h3></p>
        <img src="img/base_a.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="A-baze" id="A-baze" size="2" maxlength="5" value="<?php echo $baze_a?>">
        <br><br>
        <img src="img/base_c.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="C-baze" id="C-baze" size="2" maxlength="5" value="<?php echo $baze_c?>">
        <br><br>
        <img src="img/base_g.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="G-baze" id="G-baze" size="2" maxlength="5" value="<?php echo $baze_g?>">
        <br><br>
        <img src="img/base_t.png" name="mat" value="M" width="76" height="17">
        <input type="text" name="T-baze" id="T-baze" size="2" maxlength="5" value="<?php echo $baze_t?>">
        <br><br>

        <p><h2>&#x2462; VÝSTUP</h2></p>

        <p><h3>Počet zobrazených výsledků</h3></p>
        <img src="img/results.png" name="mat" value="M" width="76">
        <input type="text" name="vysl" id="vysl" size="2" maxlength="4" value="<?php echo $pocet_vysl?>">
        <br><br>

        <p><h3>Počet sekvencí v grafu</h3></p>
        <img src="img/plot.png" name="mat" value="M" width="76">
        <input type="text" name="grafu" id="grafu" size="2" maxlength="2" value="<?php echo $pocet_grafu?>">
        <br><br>

        <p><h2>&#x2463; VÝPOČET</h2></p>

        <p><h3>Oblast výpočtu</h3></p>
        <img src="img/matrix.png" name="mat" value="M" width="76">
        <input type="text" name="speed" id="speed" size="2" maxlength="3" value="<?php echo $speed?>">
        % <br><p><i>Menší oblast znamená rychlejší výpočet,<br>ale také možnou nepřesnost výsledku.</i></p>

        <input type= "submit" name="run" value="SPUSTIT VÝPOČET">
        <br><br>

        <?php
        // Tlačítko odeslat bylo zmáčknuto
        if (isset($_POST['run']))
        {

            // Všechny položky byly správně vyplněny
            if (!$error)
            {
                    echo '<p><h2>&#x2464; VÝSLEDKY</h2></p>';

                    // Vytvareni dat pro plotovaci funkci
                    $data_array = array();
                    $names_array = array();
                    array_push($names_array, "VSTUP");
                    
                    // Prvně beru přímý vstup
                    if (isset($_POST['StringInput']) && $_POST['StringInput'] != '')
                    {
                        $raw_string = str_replace(' ', '', $_POST['StringInput']);
                        exec("cd .. && ./prg " . $_POST['A-baze'] . " " . $_POST['C-baze'] . " " . $_POST['G-baze'] . " " . $_POST['T-baze'] . " " . $_POST['speed'] . " " . $_POST['vysl'] . " " . $raw_string, $output, $return_value);
                        array_push($data_array, strtolower($raw_string));
                    }

                    // Pokud není přímý vstup, beru soubor (ten existuje protože !$error)
                    else // if ($_FILES['fileToUpload']['tmp_name'])
                    {
                        $file_content = file_get_contents($_FILES['fileToUpload']['tmp_name']);
                        $file_content = str_replace(' ' , '' , $file_content);
                        exec("cd .. && ./prg " . $_POST['A-baze'] . " " . $_POST['C-baze'] . " " . $_POST['G-baze'] . " " . $_POST['T-baze'] . " " . $_POST['speed'] . " " . $_POST['vysl'] . " " . $file_content, $output, $return_value);
                        array_push($data_array, strtolower($file_content));
                    }

                    $i = 0;
                    while ($i < $pocet_grafu)
                    {
                        $vysl = explode(':',$output[$i]);
                        $raw_data = strtolower(file_get_contents("../test/" . $vysl[1] . ".seq"));
                        array_push($data_array, $raw_data);
                        array_push($names_array, ' ' . ($i+1));
                        $i++;
                    }

                    // Zakonceni sberu dat a vytvoreni grafu
                    echo '<img src="' . Plotting_Data($data_array, $_POST['A-baze'], $_POST['T-baze'], $_POST['C-baze'], $_POST['G-baze'], $names_array) . '" name="plot" value="M" style="width: 70%;"><br><br>';

                    // Nacteni seznamu testovanych sekvenci
                    $test = file("../test.txt", FILE_IGNORE_NEW_LINES);
                    $i = 0;
                    echo '<table><tr><th>#</th><th>Skóre</th><th>Délka</th><th>DNA sekvence</th><th style="text-align: right">Genom</th></tr>';
                    while ($i < $pocet_vysl)
                    {
                        $vysl = explode(':',$output[$i]);
                        $fullname = $test[$vysl[1]];
                        $outname  = explode(':',ParseFullname($fullname));

                        if ($i < $pocet_grafu) {echo '<tr>';}
                        else {echo '<tr>';}
                        // 1] poradi
                        echo '<td>' . ($i+1) . '.</td>';
                        // 2] DTW skore
                        echo '<td>' . $vysl[0] . '</td>';
                        // 3] delka sekvence // -2 \r\n
                        echo '<td>' . (filesize('../test/' . $vysl[1] . '.seq')-2) . '</td>';
                        // 4] DNA sekvence - lokalni odkaz
                        echo '<td style="text-align: left"><a href="../test/' . $vysl[1] . '.seq">' . rtrim($outname[0]) . '</a></td>';
                        // 5] UID (identifikator) genomu - odkaz do databaze
                        echo '<td><a href="http://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Bacteria/'. $fullname . '">' . $outname[1] . '</a></td>';
                        echo '</tr>';
                        $i++;
                    }
                    echo '</table>';
            }
        }
        ?>
        </form>
    </body>
</html>