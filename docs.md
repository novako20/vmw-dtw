# Zarovnávání DNA sekvencí převedených na časové řady

> [_Tento dokument je dostupný online na gitlabu_](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/docs.md)  
> _Odkazy zde uvedené budou platné do konce roku 2015_

#### Ondřej Novák a Pavel Verner
> novako20@fit.cvut.cz  
> vernepa1@fel.cvut.cz  
> [MI-VMV paralelka 103](https://timetable.fit.cvut.cz/old/public/cz/paralelky/C14/35/par1435506.103.html)

---

## [Zadání](https://edux.fit.cvut.cz/courses/MI-VMW/tutorials/start#seznam_projektu)

> Bioinformatika

> Zarovnávání DNA sekvencí převedených na časové řady

> DNA sekvenci lze převést na časovou řadu tak, že jednotlivým bázím A,C,G,T přiřadíme váhy (např. A:1, C:2, G:-2, T:-1) a při čtení sekvence upravujeme sumu hodnot podle právě čtené báze. Aplikace by měla umožňovat vizualizaci časových řad vygenerovaných z DNA sekvencí, váhy by měly být nastavitelné (např. ve webovém formuláři) a podobnost časových řad bude řešena pomocí DTW (dynamic time warping) distance.

## [Řešení](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/web/)

> Webová aplikace (HTML + PHP + JS), algoritmus implementován v jazyce C

> Celý projekt je dostupný veřejně na školním gitlabu:   
> [gitlab.fit.cvut.cz/novako20/vmw-dtw](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/tree/master)

> Aplikace běží na školním webdevu:  
> [webdev.fit.cvut.cz/~novako20/vmw-dtw/web](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/web/)

---

### Popis projektu
Cílem projektu je určování podobnosti mezi DNA sekvencemi za využití [DTW algoritmu](https://en.m.wikipedia.org/wiki/Dynamic_time_warping). Vstupem je sekvence tvořená ACGT řetězcem, výstupem jsou nejpodobnější sekvence z vlastního vzorku, seřazené dle vypočítaného DTW skóre.

### Způsob řešení
Základním prvkem řešení je samotný DTW algoritmus. Dynamic Time Warping je algoritmus dynamického programování, vyplňuje se tabulka o rozměru `<délka d1 vstupní sekvence> x <délka d2 testové sekvence>`, složitost (časová i prostorová) je tedy kvadratická. Jedná se vlastně o matici, která se postupně vyplňuje podle doposud vypočítaných hodnot. Výsledkem je cesta spojující protilehlé rohy matice a navíc splňující určitá další kritéria, o tom [více v přednášce](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/dtw/dtw_2.png).

Tato cesta znamená, kterak se na sebe napojují jednotlivé úseky sekvencí (lokální _natahování_), lze tedy měřit podobnost sekvencí různých délek, ne jen čistě vektorově měřením podobnosti po složkách. Ilustrace takového výsledného napojení je opět [k vidění v přednášce](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/dtw/dtw_1.png).

Jako testové sekvence jsou použity sekvence z genomů bakterií dostupných na [genbank od NCBI](http://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Bacteria/) (_National Center for Biotechnology Information_). Z každého genomu je vybrán první soubor typu _ffn_ a z toho se vezme první DNA sekvence. Postup získávání těchto testových sekvencí je také k nahlédnutí mezi zdrojovými soubory v [adresáři data](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/tree/master/data).

### Implementace
DTW algoritmus je naimplementován v jazyce C, [zdrojový kód je na gitlabu](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c). Zde nejsou použity žádné externí knihovny s výjimkou standardních [`stdlib stdio string math`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L1-L4). Implementovány jsou funkce [`dtw`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L12) a [`dtw_fast`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L62). *dtw* vyplňuje celou matici, zatímco *dtw_fast* má parametr určující oblast, která se prohledává - tím lze výpočet zrychlit, ale na druhou stranu pak nemusí být nalezeno nejlepší řešení. Oblast prohledávání je pás konstantní šírky okolo diagonály, jedná se o řešení [_Sakoe-Chiba band_](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/dtw/dtw_3.png).

Program se volá s parametrem [`speed`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/usage.txt#L4-L5) určujícím, jak velká část matice se bude vyplňovat, a teprve z toho se [vypočítá koeficient](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L73-L76) udávající šířku pásu `p = <percentage> => w = (1 - sqrt(1 - p))`. [Výraz vysvětlen](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/docs_w.png) i s vypočteným příkladem.

Všechny parametry volání jsou [popsány zde](http://webdev.fit.cvut.cz/~novako20/vmw-dtw/usage.txt). Tato hláška je součástí programu a vypíše se při nesprávném spuštění. Součástí zdrojových souborů je i [Makefile](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/Makefile) pro automatickou kompilaci a ukázkové spuštění. Aplikace se na dané platformě zkompiluje příslušným C-kompilátorem, a může tedy běžet na Windows i na Linuxu.

Samotný web je postavený jako jedna stránka [index.php](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/web/index.php) obsahující formulář pro zadání parametrů výpočtu. Formulář má celkem 4 sekce k vyplnění: _vstup_ (vstupní sekvence), _váhy_ (váhy jednotlivých bází), _výstup_ (počet sekvencí vykreslených v grafu a celkově počet zobrazených výsledků) a _výpočet_ (procentuální oblast pro zrychlený výpočet). Jedná se pouze html kód, inline stylování, javascript na předvyplnění formuláře a konečně php skripty pro dynamické generování obsahu stránky na základě odeslaného formuláře. Navíc použita externí php knihovna [pChart](http://wiki.pchart.net/) pro vykreslení grafu. Požadavkem na běh je pak pouze funkční webserver s podporou php.

### Příklad výstupu
[Ukázkový vstup a zobrazení výsledku v aplikaci](http://webdev.fit.cvut.cz/~novako20/vmw-dtw/web/showcase.png)

Stejného výsledku lze dosáhnout na [webu aplikace](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/web/) po zmáčknutí tlačítka `UKÁZKOVÝ VSTUP` a následně `SPUSTIT VÝPOČET`

### Experimentální sekce
Základním parametrem ovlivňujícím rychlost (ale i přesnost) je již výše diskutovaný parametr [`speed`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/usage.txt#L4-L5). Jeho vliv je zdokumentován ve výstupem testovacího skiptu [`time.sh`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/time.sh), ten ukládá výstupy jednotlivých běhů do společného [adresáře](http://webdev.fit.cvut.cz/~novako20/vmw-dtw/time/), a lze tak zjistit rozdíly mezi vypočtenými hodnotami v rámci jedné zadané vstupní sekvence. Samotný skript pak poskytuje [výstup v podobě doby běhu](http://webdev.fit.cvut.cz/~novako20/vmw-dtw/time/time.txt), která je ve výše uvedeném adresáři taktéž k vidění.

Zde je navíc vidět podstatné zlepšení časů při kompilaci se [zapnutou optimalizací](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/Makefile#L3). Pro porovnání je zobrazen i výsledek měření nasazené aplikace na webdevu - celkový čas `real` je lepší, nicméně výpočetní čas `user` vychází lépe při lokálním testování. Při běhu programu pak samozřejmě záleží také na omezeních uživatelského účtu, pod kterým se spouštějí php skripty (který spustí tento výpočetní program).

Posledním naimplementovaným vylepšením je [přeskakování shodných sekvencí](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/commit/ba62439ce8f743b0a0ffb95fe28f75c368b2c436). Ve výsledcích se totiž objevovaly shodné výsledky pro několik po sobě jdoucích testových sekvencí. To je způsobené shodou DNA sekvence na začatku genomu, která byla použita jako vzorek pro testové sekvence. Jelikož jsou sekvence seřazené dle jména, lze předpokládat, že shodné sekvence se budou vyskytovat za sebou. Po přečtení následující sekvence se tedy zkontroluje, zda neodpovídá předcházející sekvenci, a pokud ano, předchází bez dalšího výpočtu se na další testový vzorek.

### Diskuze
Algoritmus DTW je dobře známý a jeho implementace přímočará, nicméně i tam lze dosáhnout podstatného zlepšení, a to paralelizováním výpočtu. Výpočty pro jednotlivé testové sekvence jsou vzájemně nezávislé, akorát by se zvýšila paměťová náročnost, jelikož každá právě testovaná sekvence by musela mít vlastní matici pro výpočet DTW skóre.

Veškerá další vylepšení tedy spočívají v optimalizaci věcí okolo. Jedná se zejména o čtení ze souborového systému. Všechny testové sekvence by mohly být v jednom souboru, případně lze uvažovat i o vlastním binárním kódování `A C G T => 00 01 10 11`, tím se zmenší velikost souboru. Pro maximální rychlost lze úplně eliminovat čtení z disku vypsáním sekvencí přímo do zdrojového souboru. Dále by šlo efektivně řešit vypisování výsledků. Nyní se pro každý jednotlivý výsledek projde celé pole výsledků. Při větším počtu zobrazených výsledků (případně všechny) je lepší nejprve pole seřadit a pak teprve sekvenčně vypsat. Kde je optimální hranice mezi postupným procházením a seřazením lze zjistit experimentálně.

Co se týká webové prezentace, lze dále rozvinout vizualizaci. Nyní je implementováno generování obrázku ze zdrojových dat. Graf by mohl být vytvořen i interaktivní nativně pomocí javascriptu. Navíc by šlo vizualizaci dále informačně obohatit například zobrazením [vodícíh linek](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/dtw/dtw_1.png), které ozřejmí, jakým způsobem jsou na sebe sekvence navázány (tuto informaci dává právě ona zvolená cesta ve výpočetní matici).

### Závěr
Cílem zde bylo seznámit se s konkrétní technikou nacházení podobností (DTW algoritmus) a aplikovat ji v konkrétní oblasti použítí (DNA sekvence v bioinformatice). Po seznámení s principem fungování DTW jsme naimplementovali základní plně kvadratickou verzi [`dtw`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L12) a prokročilou rychlejší verzi [`dtw_fast`](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/blob/master/dtw.c#L62). Rychlejší verze však nemusí najít optimální výsledek, jelikož nebere v úvahu všechna možná řešení.

Webovou aplikaci pak tvoří formulář, kam se zadá vstupní sekvence spolu s dalšími parametry výpočtu. Po odeslání se na pozadí spustí program implementující DTW algoritmus a výsledky jsou následně zobrazeny ve formě grafu a tabulky s vypsanými sekvencemi seřazenými dle vypočítaného skóre podobnosti. 

Pro vývoj projektu byla využita školní infrastura: [gitlab](https://gitlab.fit.cvut.cz/novako20/vmw-dtw/tree/master) pro správu zdrojových souborů a [webdev](https://webdev.fit.cvut.cz/~novako20/vmw-dtw/web/) pro nasazení aplikace.
