$files = dir "ftp.ncbi.nih.gov" -Recurse -Filter "*.ffn" | foreach {$_.fullname}

$last  = ""
$list  = @()
$listf = @()
foreach ($file in $files) {
    $base = $file -replace '^(.+)\\.+?$','$1'
    if (($file -replace '^.+\\Bacteria\\(.).+$','$1') -cmatch '[^A-Z]') {continue}
    if ($file -match 'Croceibacter_atlanticus') {continue} # 3x larger than rest
    if ($last -ne $base) {
        $list += ($file -replace '^.+?\\Bacteria\\')
        $listf += $file
    }
    $last = $base
}

sc list.txt $list

for ($i = 0; $i -lt $listf.count; $i++) {
    write-host "$($i+1) / $($listf.count)"
    $n = 1
    $sequence = ""
    $line = (gc $listf[$i] | select -Index $n)
    while ($line -match '^[ACGT]') {
        $sequence += $line
        $n++
        $line = (gc $listf[$i] | select -Index $n)
    }
    sc "test\$i.seq" $sequence
}
