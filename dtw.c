#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#define TEST_CNT 2740 // 2740 // # of test sequences to compare (up to 9999)
#define TEST_MAX 8992 // 8992 // length of longest test sequence + 1
// #define RESULTS   100 // # of results to display

// #define DEBUG

int dtw (int ** dtwm, int * seq1, int seq1_len, int * seq2, int seq2_len) {
	int i, j;
	int dif, min;

	// comments in dtw_fast

	dif = seq1[0] - seq2[0];
	if (dif < 0) dif = -dif;
	dtwm[0][0] = dif;

	for (i = 1; i < seq1_len; i++) {
		dif = seq1[i] - seq2[0];
		if (dif < 0) dif = -dif;
		dtwm[i][0] = dtwm[i-1][0] + dif;
	}

	for (j = 1; j < seq2_len; j++) {
		dif = seq1[0] - seq2[j];
		if (dif < 0) dif = -dif;
		dtwm[0][j] = dtwm[0][j-1] + dif;
	}

	for (i = 1; i < seq1_len; i++) {
		for (j = 1; j < seq2_len; j++) {
			dif = seq1[i] - seq2[j];
			if (dif < 0) dif = -dif;
			min = dtwm[i-1][j-1];
			if (dtwm[i][j-1] < min) min = dtwm[i][j-1];
			if (dtwm[i-1][j] < min) min = dtwm[i-1][j];
			dtwm[i][j] = min + dif;
		}
	}

	#ifdef DEBUG
	char print; // bolean 0/1
	for (i = 0; i < seq1_len; i++) {
		print = 0;
		if (! (i % 100) || i+1 == seq1_len) {print = 1; printf("%d: ", i);}
		if (print) {
			for (j = 0; j < seq2_len; j++) {
				if (! (j % 100) || j+1 == seq2_len) printf("%d ", dtwm[i][j]);
			}
			printf("\n");
		}
	}
	#endif

	return dtwm[seq1_len-1][seq2_len-1];
}

int dtw_fast (int speed, int ** dtwm, int * seq1, int seq1_len, int * seq2, int seq2_len) {
	if (speed < 5 || speed > 95) return dtw(dtwm, seq1, seq1_len, seq2, seq2_len);

	int i, j;
	int dif, min;

	double w = (double) speed / 100;         // speed of computation, lower => faster but not necessarily precise
	double r = (double) seq2_len / seq1_len; // ratio of lengths of given sequences
	int begin, end;  // first and last index in current row to be computed
	int first, last; // first and last index which was computed in preceding row

	// recompute to match desired speed
	// => <speed> % of matrix is filled
	// (expression explained in docs)
	w = (1 - sqrt(1-w));

	// index [0][0] of DTW matrix
	dif = seq1[0] - seq2[0]; // metric is absolute value |x-y|
	if (dif < 0) dif = -dif;
	dtwm[0][0] = dif;

	// first column of DTW matrix
	for (i = 1; (i < seq1_len) && (r*i - w*seq2_len <= 0); i++) {
		dif = seq1[i] - seq2[0];
		if (dif < 0) dif = -dif;
		dtwm[i][0] = dtwm[i-1][0] + dif;
	}

	// first row of DTW matrix
	for (j = 1; (j < seq2_len) && (j <= w*seq2_len); j++) {
		dif = seq1[0] - seq2[j];
		if (dif < 0) dif = -dif;
		dtwm[0][j] = dtwm[0][j-1] + dif;
	}

	// [first,last)
	first = 0;
	last = j;
	for (i = 1; i < seq1_len; i++) {
		// going through each row (each i)
		// from diagonal (r*i) you can go +/- w*seq2_len
		// w is our speed parameter (larger w => more computations)
		begin = r*i - w*seq2_len;
		if (begin < 1) begin = 1;
		end = r*i + w*seq2_len + 1;
		if (end > seq2_len) end = seq2_len;

		for (j = begin; j < end; j++) {
			dif = seq1[i] - seq2[j];
			if (dif < 0) dif = -dif;

			// get minimum of previous three cells
			// but some of them might not be computed
			if (j > begin) min = dtwm[i][j-1];
			else min = -1; // min not set yet
			if (j >= first && j <= last) {
				if (j > first && (min == -1 || dtwm[i-1][j-1] < min)) min = dtwm[i-1][j-1];
				if (j < last  && (min == -1 || dtwm[i-1][j]   < min)) min = dtwm[i-1][j];
			}
			// always at least one previous cell computed (think about it)
			// => min is set correctly
			dtwm[i][j] = min + dif;
		}
		first = begin;
		last = j;
	}

	#ifdef DEBUG
	char print;
	for (i = 0; i < seq1_len; i++) {
		print = 0;
		if (! (i % 100) || i+1 == seq1_len) {print = 1; printf("%d: ", i);}
		if (print) {
			for (j = 0; j < seq2_len; j++) {
				if (! (j % 100) || j+1 == seq2_len) printf("%d ", dtwm[i][j]);
			}
			printf("\n");
		}
	}
	#endif

	return dtwm[seq1_len-1][seq2_len-1];
}

void sum (const char * arg, int * seq, int seq_len, int * tab) {
	int i, sum;

	sum = 0;
	for (i = 0; i < seq_len; i++) {
		// ASCII codes / ascii + 32
		// A=65 / C=67 / G=71  / T=84
		// a=97 / c=99 / g=103 / t=116
		switch (arg[i]) {
			case 65:
			case 97:
				sum += tab[0];
				break;
			case 67:
			case 99:
				sum += tab[1];
				break;
			case 71:
			case 103:
				sum += tab[2];
				break;
			case 84:
			case 116:
				sum += tab[3];
				break;
			#ifdef DEBUG
			default:
				fprintf(stderr, "Error sequence letter: %c (position: %d)\n", arg[i], i);
			#endif
		}
		seq[i] = sum;
	}
}

int main (int argc, char * argv[]) {
	if (argc != 8) {
		fprintf(
			stderr,
			"Usage: %s <A> <C> <G> <T> <SPEED> <RESULTS> <SEQUENCE>\n\n"
			"\t<A/C/G/T>  - int values for sequence conversion\n"
			"\t<SPEED>    - %% meaning how far the path can go off the diagonal,\n"
			"\t             from 5 (very fast, not precise) to 100 (full computation)\n"
			"\t<RESULTS>  - # of results to output as <DTW score>:<sequence number>,\n"
			"\t             currently %d test sequences available\n"
			"\t<SEQUENCE> - DNA sequence made of A/C/G/T letters\n",
			argv[0], TEST_CNT
		);
		return 1;
	}

	int i, j, x;

	int speed;            // speed (but also precision) of computation
	int tab[4];           // A/C/G/T conversion values
	int results;          // # of results to output
	int res[TEST_CNT];    // resulting DTW score for each comparison
	int * seq, seq_len;	  // input sequence (argument)
	int * test, test_len; // current test sequence
	int test_len_lag;     // previous test sequence
	int ** dtwm;          // DTW matrix for computation

	FILE * fp = NULL;         // file containing test DNA sequence
	char test_path[16];       // test sequence "./test/0000.seq"
	char test_str0[TEST_MAX]; // array to store test sequence
	char test_str1[TEST_MAX]; // array to check if next seq is equal
	char * test_str = NULL;   // pointing to one of test_str{0,1}

	seq_len = strlen(argv[argc-1]);
	seq  = (int *)  malloc (sizeof(int)  * seq_len);
	test = (int *)  malloc (sizeof(int)  * TEST_MAX);
	dtwm = (int **) malloc (sizeof(int*) * seq_len);
	for (i = 0; i < seq_len; i++) dtwm[i] = (int *) malloc (sizeof(int) * TEST_MAX);

	for (i = 0; i < 4; i++) tab[i] = atoi(argv[i+1]);
	speed   = atoi(argv[i+1]);
	results = atoi(argv[i+2]);
	if (results < 0) results = 10;
	if (results > TEST_CNT) results = TEST_CNT;

	test_len_lag = 0;
	sum(argv[argc-1], seq, seq_len, tab);
	for (i = 0; i < TEST_CNT; i++) {
		sprintf(test_path, "./test/%d.seq", i);
		fp = fopen(test_path, "r");
		test_str = (i % 2) ? test_str1 : test_str0;
		fscanf(fp, "%s", test_str);
		fclose(fp);
		test_len = strlen(test_str);
		if (test_len == test_len_lag && !strcmp(test_str0, test_str1)) {
			// skip sequence if equal to previous
			res[i] = -1;
			continue;
		}
		test_len_lag = test_len;
		sum(test_str, test, test_len, tab);
		res[i] = dtw_fast(speed, dtwm, seq, seq_len, test, test_len);
		#ifdef DEBUG
		printf("%d / %d\n", i+1, TEST_CNT);
		#endif
	}

	for (j = 0; j < results; j++) {
		x = 0;
		while (x < TEST_CNT && res[x] < 0) x++;
		for (i = x + 1; i < TEST_CNT; i++) {
			if (res[i] < res[x] && res[i] >= 0) x = i;
		}
		// x can be == TEST_CNT because of skipping
		if (x == TEST_CNT) printf("-1:-1\n");
		else {
			// <DTW score>:<test sequence number>
			printf("%d:%d\n", res[x], x);
			res[x] = -1;
		}
	}

	for (i = 0; i < seq_len; i++) free(dtwm[i]);
	free(dtwm);
	free(test);
	free(seq);
	return 0;
}
